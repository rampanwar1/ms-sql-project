# Install sql server on docker 

sudo docker run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=Admin@123" \                                                                               130 ↵ ──(Mon,
   -p 1433:1433 --name sql1 --hostname mysql-server \
   -d \
   mcr.microsoft.com/mssql/server:2022-latest

# Download Cli on docker 

docker run -it -d --name sql-cli --restart always dbcliorg/mssql-cli:latest && \
docker exec -it sql-cli bash 

# Connect Database 
mssql-cli -S 172.17.0.2 -U SA -P Admin@12

# Basic command
\ld  #List commnad 
use DATABSE _NAME;  #Select database
SELECT * FROM "dbo"."DataOps_team" #List column from database
# Create Database 
Query:-  create database TMDC


# Create Table
create table DataOps_team(
	Name varchar(20),
	Experience_years float,
	City varchar(20),
	previouse_company_name varchar(20)
)

# Get column in table

select Column_name 
from Information_schema.columns 
where Table_name like 'DataOps_team'

# Add Data in tables

insert into DataOps_team ( Name,Experience_years,City,previouse_company_name) VALUES( 'Chetan Gehlot',8,'Nagda','47Billion' );
insert into DataOps_team ( Name,Experience_years ,City,previouse_company_name ) VALUES ('Amit Jain',4,'Indore','synergytop inc' );
insert INTO DataOps_team ( Name,Experience_years,City,previouse_company_name ) VALUES ( 'Shubhranil Ghose',4,'Kolkata','Tech Mahindra' );
INSERT INTO DataOps_team ( Name,Experience_years ,City ,previouse_company_name ) VALUES ( 'Nitin Mandloi',10,'Badwani','Codiant')
INSERT INTO DataOps_team (Name,Experience_years,City,previouse_company_name) VALUES ( 'Pranjal Laad',1,'Khandwa','47Billion' )

# Insert Another Data

insert INTO "dbo"."City" VALUES ('India','Madhya Pradesh','Indore')
                                     or
# Another way to import database
INSERT INTO DataOps_team VALUES	
( 'Shyam',6,'HYB','NUll' ),( 'John',6,'HYB','NUll' ),
( 'Harry',9,'HYB','NUll' ),( 'Natasha',6,'HYB','NUll'),
( 'any',7,'HYB','NUll' ),( 'Johny',6,'HYB','NUll' )


# Get Data of sql table

SELECT * FROM DataOps_team;

# Get specific Table column for table ( Get Name Column only)
SELECT name from DataOps_team;

# Select 2 Column from Database table 
SELECT Name,City from DataOps_team;

# Add  new column

ALTER TABLE City
ADD test varchar(10);

# Delete Column 

ALTER TABLE City
DROP COLUMN Email;

ALTER TABLE City
DROP COLUMN test;

# Ingnore Dubplicate value from colume.

Use:- DISTINCT 

SELECT DISTINCT district FROM CITY